# README #

This repository is made for practising basic OpenGL tools.
Used material: Computer Graphics Laboratory, Eötvös Loránd University

OpenGL version: 4.3

### Setup

The repository uses the following libraries:
* SDL
* OpenGL
* GLM

To set up dependencies, download:
* [OGLPack](http://cg.elte.hu/~bsc_cg/resources/OGLPack.zip)

## Configuration

Extract OGLPack to a folder with preferred <path>
Use command prompt: create a virtual T: disc

```bash
subst t: <path>
```

To delete the virtual disk

```bash
subst /D t:
```

On Visual Studio:

1. Project/Properties -> Configuration Properties
2. Set configuration to All Configurations
3. Configuration Properties/VC++ directories

	* Set include files: *T:\OGLPack\include*
	* Set lib files: *T:\OGLPack\lib\x86*

4. Configuration Properties/Debugging

	* Set environment: *PATH=T:\OGLPack\bin\x86;%PATH%*

5. Linker/Input/Additional dependencies:

	* SDL2.lib
	* SDL2main.lib
	* SDL2_image.lib
	* glew32.lib
	* glu32.lib
	* opengl32.lib


### Contribution guidelines

* Code review please!

### Who do I talk to?

* Repo owner or admin