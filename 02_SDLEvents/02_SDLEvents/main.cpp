#include <SDL.h>
#include <iostream>
#include <time.h>  

void exitProgram()
{
	std::cout << "Press any button to exit..." << std::endl;
	std::cin.get();
}

int main(int argc, char* args[])
{
	// Setup system to call atexit() before exit
	atexit(exitProgram);

	//
	// Step 1: initialize SDL
	//

	// only set the video subsystem, if there is a problem, indicate and exit
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		std::cout << "[Start SDL]Unable to initialize SDL: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// Step 2: create window to draw on
	//

	SDL_Window *win = nullptr;
	win = SDL_CreateWindow("Hello SDL!",				// window header
		100,						// left-top X coordinate of window
		100,						// left-top Y coordinate of window
		640,						// width
		480,						// height
		SDL_WINDOW_SHOWN);			// display properties

									//  if there is a problem, indicate and exit
	if (win == nullptr)
	{
		std::cout << "[Create window]Unable to initialize SDL: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// Step 3: Create a renderer: a 'drawing function'
	//

    SDL_Renderer *ren = nullptr;
    ren = SDL_CreateRenderer(	win, // to which window do we add render
								-1,  // on which index is the renderer
								SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);	// our expectations
    if (ren == nullptr)
	{
        std::cout << "[Create renderer while initializing SDL: " << SDL_GetError() << std::endl;
        return 1;
    }

	//
	// Step 4: Create main event cycle
	// 

	// should the program end?
	bool quit = false;
	// event to process
	SDL_Event ev;
	// X and Y coords of the mouse
	Sint32 mouseX = 0, mouseY = 0;
	// for excercises 
	bool left = true;
	

	while (!quit)
	{
		// while there is event, process
		while ( SDL_PollEvent(&ev) )
		{
			switch (ev.type)
			{
				case SDL_QUIT:
					quit = true;
					break;
				case SDL_KEYDOWN:
					if (ev.key.keysym.sym == SDLK_ESCAPE) {
						quit = true;
					}
					break;
				case SDL_MOUSEMOTION:
					mouseX = ev.motion.x;
					mouseY = ev.motion.y;
					break;
				case SDL_MOUSEBUTTONUP:
					// SDL_BUTTON_LEFT, SDL_BUTTON_MIDDLE, 
					// SDL_BUTTON_RIGHT, SDL_BUTTON_WHEELUP, SDL_BUTTON_WHEELDOWN
					// Excercise 2: ha a user a bal eg�rgombot nyomja meg akkor a t�glalap sz�ne v�ltson pirosra,
					//    ha a jobb eg�rgombot, akkor k�kre
					if (ev.button.button == SDL_BUTTON_LEFT) {
						left = true;
					}
					if (ev.button.button == SDL_BUTTON_RIGHT) {
						left = false;
					}
					break;
			}
		}


	// 1. feladat: az eltelt id� f�ggv�ny�ben periodikusan n�jj�n �s cs�kkenjen
	// az eg�rmutat� k�z�ppontj�val kirajzolt n�gysz�g
	// elapsed time
		int t = SDL_GetTicks() / 1000;
	// t: seconds. 1 second ->2pi. +1 [0;2]. 25 * -> zoom 
		double diff = 100 * (sin( t * 2 * M_PI / 3) + 1) ;

	// clear background to white
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	SDL_RenderClear(ren);

	// actual drawing color is green, draw a line
	SDL_SetRenderDrawColor(ren,	// address of the renderer
		0,		// R
		255,	// G
		0,		// B
		255);	// transparency

	SDL_RenderDrawLine(ren,	// address of the renderer
		0, 0, // vonal kezd�pontj�nak (x,y) koordin�t�i
		mouseX, mouseY);// vonal v�gpontj�nak (x,y) koordin�t�i
	// define a 20x20 rectangle
		SDL_Rect cursor_rect;
		cursor_rect.x = mouseX - (20 + diff) /2;
		cursor_rect.y = mouseY - (20 + diff) / 2;
		cursor_rect.w = 20 + diff;
		cursor_rect.h = 20 + diff;
	// legyen a kit�lt�si sz�n piros
		if (left) {
			SDL_SetRenderDrawColor(ren, 225, 0, 0, 255);
		} else {
			SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
		}
		SDL_RenderFillRect(ren, &cursor_rect);

	// 3. feladat: rajzolj ki egy 50 sugar� k�rvonalat az eg�rmutat� k�r�!
	// seg�ts�g: haszn�ld a SDL_RenderDrawLines()-t
		SDL_Point points[101];
		for (int i = 0; i <= 100; ++i)
		{
			double alpha = i * 2.0 * M_PI / 100;
			points[i].x = mouseX + 100 * cos(alpha);
			points[i].y = mouseY + 100 * sin(alpha);
		}

		 SDL_RenderDrawLines(ren, points, 101);

	// jelen�ts�k meg a backbuffer tartalm�t
		SDL_RenderPresent(ren);
	}

	//
	// 4. l�p�s: l�pj�nk ki
	// 

	SDL_DestroyRenderer( ren );
	SDL_DestroyWindow( win );

	SDL_Quit();

	return 0;
}