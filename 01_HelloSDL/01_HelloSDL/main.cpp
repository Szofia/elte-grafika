#include <SDL.h>
#include <iostream>
#include <array>
#include <vector>

// return array of letter points for 25x50px letters
std::vector<SDL_Point> drawLetter(char letter, SDL_Point coord) {

	switch (letter) {
		case 'h': {
			std::vector<SDL_Point> points{
				{ coord.x, coord.y },
				{ coord.x, coord.y + 50 },
				{ coord.x, coord.y + 25 },
				{ coord.x + 25, coord.y + 25 },
				{ coord.x + 25, coord.y },
				{ coord.x + 25, coord.y + 50 }
			};
			return points;
		}
		case 'e': {
			std::vector<SDL_Point> points{
				{ coord.x, coord.y },
				{ coord.x + 25, coord.y },
				{ coord.x, coord.y },
				{ coord.x, coord.y + 25},
				{ coord.x + 25, coord.y + 25 },
				{ coord.x, coord.y + 25 },
				{ coord.x, coord.y + 50 },
				{ coord.x + 25, coord.y + 50 }
			};
			return points;
		}
		case 'l': {
			std::vector<SDL_Point> points{
				{ coord.x, coord.y },
				{ coord.x, coord.y + 50 },
				{ coord.x + 25, coord.y + 50 }
			};
			return points;
		}
		case 'o': {
			std::vector<SDL_Point> points{
				{ coord.x, coord.y },
				{ coord.x, coord.y + 50 },
				{ coord.x + 25, coord.y + 50 },
				{ coord.x + 25, coord.y },
				{ coord.x, coord.y }
			};
			return points;
		}
	}
}

void exitProgram()
{
	std::cout << "Kil�p�shez nyomj meg egy billenty�t..." << std::endl;
	std::cin.get();
}

int main(int argc, char* args[])
{
	// Setup system to call atexit() before exit
	atexit(exitProgram);

	//
	// Step 1: initialize SDL
	//

	// only set the video subsystem, if there is a problem, indicate and exit
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		std::cout << "[Start SDL]Unable to initialize SDL: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// Step 2: create window to draw on
	//

	SDL_Window *win = nullptr;
	win = SDL_CreateWindow("Hello SDL!",				// window header
		100,						// left-top X coordinate of window
		100,						// left-top Y coordinate of window
		640,						// width
		480,						// height
		SDL_WINDOW_SHOWN);			// display properties

	//  if there is a problem, indicate and exit
	if (win == nullptr)
	{
		std::cout << "[Create window]Unable to initialize SDL: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// 3. l�p�s: hozzunk l�tre egy renderel�t, rajzol�t
	//

	SDL_Renderer *ren = nullptr;
	ren = SDL_CreateRenderer(win, // melyik ablakhoz rendelj�k hozz� a renderert
		-1,  // melyik index� renderert inicializ�ljuka
			 // a -1 a harmadik param�terben meghat�rozott ig�nyeinknek megfelel� els� renderel�t jelenti
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);	// az ig�nyeink, azaz
																// hardveresen gyors�tott �s vsync-et bev�r�
	if (ren == nullptr)
	{
		std::cout << "[Renderer l�trehoz�sa]Hiba az SDL inicializ�l�sa k�zben: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// 4. l�p�s: t�r�lj�k az ablak h�tt�rsz�n�t �s v�rjunk 2 m�sodpercet
	//

	// aktu�lis rajzol�si sz�n legyen fekete �s t�r�lj�k az aktu�lis rajzol�si sz�nnel az ablak klienster�let�t
	SDL_SetRenderDrawColor(ren,	// melyik renderel�nek �ll�tjuk be az aktu�lis rajzol�si sz�n�t
		0,	// v�r�s intenzit�s - 8 bites el�jel n�lk�li eg�sz sz�m
		0,		// z�ld intenzit�s - 8 bites el�jel n�lk�li eg�sz sz�m
		0,		// k�k intenzit�s - 8 bites el�jel n�lk�li eg�sz sz�m
		255);	// �tl�tsz�s�g - 8 bites el�jel n�lk�li eg�sz sz�m
	SDL_RenderClear(ren);

	// aktu�lis rajzol�si sz�n legyen z�ld �s rajzoljunk ki egy vonalat
	SDL_SetRenderDrawColor(ren,	// renderer c�me, aminek a rajzol�si sz�n�t be akarjuk �ll�tani
		0,		// piros
		255,	// z�ld
		0,		// k�k
		255);	// �tl�tszatlans�g

	SDL_RenderDrawLine(ren,	// renderer c�me, ahov� vonalat akarunk rajzolni
		10, 10, // vonal kezd�pontj�nak (x,y) koordin�t�i
		10, 60);// vonal v�gpontj�nak (x,y) koordin�t�i

	// 1. feladat: eg�sz�ts�k ki a fenti vonalat egy H bet�v�!
	SDL_RenderDrawLine(ren,	// renderer c�me, ahov� vonalat akarunk rajzolni
		35, 10, // vonal kezd�pontj�nak (x,y) koordin�t�i
		35, 60);// vonal v�gpontj�nak (x,y) koordin�t�i

	SDL_RenderDrawLine(ren,	// renderer c�me, ahov� vonalat akarunk rajzolni
		10, 35, // vonal kezd�pontj�nak (x,y) koordin�t�i
		30, 35);// vonal v�gpontj�nak (x,y) koordin�t�i

	// 2. feladat: �rjuk ki a "HELLO" sz�veget a k�perny�re! Ehhez haszn�lhat� a
	// SDL_RenderDrawLines( <renderer ptr>, <SDL_Point t�mb>, <pontok sz�ma>); parancs!
	SDL_RenderDrawLines(ren,
		drawLetter('h', { 100, 10 }).data(),
		drawLetter('h', { 100, 10 }).size()
	);
	SDL_RenderDrawLines(ren,
		drawLetter('e', { 130, 10 }).data(),
		drawLetter('e', { 130, 10 }).size()
	);
	SDL_RenderDrawLines(ren,
		drawLetter('l', { 160, 10 }).data(),
		drawLetter('l', { 160, 10 }).size()
	);
	SDL_RenderDrawLines(ren,
		drawLetter('l', { 190, 10 }).data(),
		drawLetter('l', { 190, 10 }).size()
	);
	SDL_RenderDrawLines(ren,
		drawLetter('o', { 220, 10 }).data(),
		drawLetter('o', { 220, 10 }).size()
	);


	// 3. feladat: 2 m�sodpercenk�nt v�ltozzon a h�tt�r sz�ne! El�sz�r legyen piros, azt�n z�ld �s v�g�l k�k,
	// majd l�pjen ki a program!
	// jelen�ts�k meg a backbuffer tartalm�t
	SDL_RenderPresent(ren);

	// wait for 2 seconds
	SDL_Delay(2000);

	// display red
	SDL_SetRenderDrawColor(ren,
		255,
		0,	
		0,
		255);	
	SDL_RenderClear(ren);
	SDL_RenderPresent(ren);
	SDL_Delay(2000);

	// display green
	SDL_SetRenderDrawColor(ren,
		0,
		255,
		0,
		255
	);
	SDL_RenderClear(ren);
	SDL_RenderPresent(ren);
	SDL_Delay(2000);

	// display blue
	SDL_SetRenderDrawColor(ren,	
		0,
		0,
		255,
		255
	);
	SDL_RenderClear(ren);
	SDL_RenderPresent(ren);
	SDL_Delay(2000);

	//
	// Step 5: exit
	// 

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);

	SDL_Quit();

	return 0;
}